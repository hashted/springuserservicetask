package com.ilin.bostongene.springuserservicetask.repository;

import com.ilin.bostongene.springuserservicetask.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    User findByEmail(String email);
    void deleteById(UUID uuid);
}
