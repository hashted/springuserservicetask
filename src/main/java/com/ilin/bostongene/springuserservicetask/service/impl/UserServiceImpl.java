package com.ilin.bostongene.springuserservicetask.service.impl;

import com.ilin.bostongene.springuserservicetask.entity.User;
import com.ilin.bostongene.springuserservicetask.repository.UserRepository;
import com.ilin.bostongene.springuserservicetask.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserServiceImpl(@Qualifier("userRepository") UserRepository userRepository,
                           BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public UUID saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        UUID uuid = userRepository.save(user).getUuid();
        return uuid;
    }

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User findUserById(UUID uuid) {
        try{
            User finded = userRepository.findById(uuid).get();
            return finded;
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    @Override
    public void deleteUserById(UUID uuid) {
        userRepository.deleteById(uuid);
    }
}
