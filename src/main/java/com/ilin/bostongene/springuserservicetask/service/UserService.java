package com.ilin.bostongene.springuserservicetask.service;

import com.ilin.bostongene.springuserservicetask.entity.User;

import java.util.Optional;
import java.util.UUID;

public interface UserService {

    UUID saveUser(User user);
    User findUserByEmail(String email);
    User findUserById(UUID uuid);
    void deleteUserById(UUID uuid);
}
