package com.ilin.bostongene.springuserservicetask.controller;

import com.ilin.bostongene.springuserservicetask.entity.User;
import com.ilin.bostongene.springuserservicetask.service.UserService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
public class UserRestController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ResponseEntity<String> add(@Valid @RequestBody User user) {

        UUID uuid = userService.saveUser(user);

        HttpHeaders headers = new HttpHeaders();
        headers.add("content-type", "application/json");

        ResponseEntity<String> responseEntity = ResponseEntity.created(null).headers(headers)
                .body(new JSONObject().put("uuid", uuid).toString());

        return responseEntity;
    }

    @RequestMapping(value = "/users/{email}", method = RequestMethod.GET)
    public ResponseEntity<User> find(@PathVariable String email) {
        User user = userService.findUserByEmail(email);

        if(user == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(user);
        }
    }

    @RequestMapping(value = "/users/{uuid}", method = RequestMethod.DELETE)
    public  ResponseEntity<Void> delete(@PathVariable UUID uuid) {
        User user = userService.findUserById(uuid);

        if(user == null) {
            return ResponseEntity.notFound().build();
        } else {
            userService.deleteUserById(uuid);
            return ResponseEntity.ok().build();
        }
    }
}
