Source code for BostonGene Junior task #2 solved by Nikolay Ilin.

Maven based project.

# Getting started
Compile the application sources:
```sh
mvn compile
```
Create a JAR file:
```sh
mvn package
```
Run the application:
```sh
java -jar SpringUserServiceTask.jar
```

# Using examples (for Windows command line)
To create a new user:
```
curl -v --request POST --header "Content-Type: application/json" ^
  --data "{\"email\":\"name@mail.com\",\"password\":\"1234\",\"surname\":\"Dowson\",\"name\":\"Bill\",\"dateOfBirth\":\"1991-10-26\"}" ^
  http://localhost:8080/users/
```

To find a user by email:
```
curl -v --request GET http://localhost:8080/users/name@mail.com
```


To delete a user by UUID:
```
curl -v --request DELETE http://localhost:8080/users/b17d36b7-a2da-4b04-9083-1d9ea78a3b3c
```